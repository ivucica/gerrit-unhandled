<?php
$gerrit_base = "https://badc0de.net/r"; // No trailing slash please.
?><html><head><title>Gerrit unhandled comments</title>

<!--

* Copyright (c) 2015, Ivan Vucica
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of Ivan Vucica nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY Ivan Vucica ''AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL Ivan Vucica BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Full source code available if you change .php into .phps or upon request

-->

<style>
.done {
  background-color: grey;
}
.thread {
  border: 1px black solid;
  margin-bottom: 10px;
  width: 50%;
}
</style>
<script>
function done_hide() {
    var elements = document.querySelectorAll('.done');
    for(var i=0; i<elements.length; i++){
        elements[i].style.display = 'none';
    }
}
function done_show() {
    var elements = document.querySelectorAll('.done');
    for(var i=0; i<elements.length; i++){
        elements[i].style.display = 'inherit';
    }
}
</script>
</head>
<body>
<a name="top"></a><h1>Gerrit unhandled comments</h1>
Accessing installation at URL <a href="<?=$gerrit_base?>/"><?=$gerrit_base?>/</a><br>
<br>
<?php
function unhandled_comments($gerrit_url, $change) {
    if(!in_array(substr($gerrit_url, 0, 7), array('http://', 'https:/'))) {
	return;
    }
    $change = intval($change);

    echo '<a href="javascript:done_hide();">done_hide</a> - ';
    echo '<a href="javascript:done_show();">done_show</a><br>';

    echo '<h2>Change <a href="' . $gerrit_url . '/' . $change . '" target="_blank">' . $change . '</a></h2>';
    $opts = array(
        'http'=>array(
            'method'=>'GET',
            'header'=>"Accept: application/json\r\n"
        ),
        'ssl'=>array(
            'verify_peer'=>false
        )
    );
    $context = stream_context_create($opts);
    $chg = file_get_contents($gerrit_url . "/changes/" . $change 
	                     . "?o=ALL_REVISIONS",
                             false, $context);
    $chg = json_decode(substr($chg,4));

    $revisions = array();

    function comment_comp($a, $b) {
        return $a->updated > $b->updated ? 1 : -1;
    }
    function rev_comp($a, $b) {
        if(intval($a->_number) == intval($b->_number))
            return 0;
        //return intval($a->_number) < intval($b->_number) ? -1 : 1;
        return intval($a->_number) > intval($b->_number) ? -1 : 1;
    }
    $revisions = (array)$chg->revisions;
    usort($revisions, "rev_comp");
    $patchsets = array();

    if(!count($revisions)) return;

    foreach ($revisions as $revprop) {

	$revcom = file_get_contents(
	    $gerrit_url . "/changes/" . $change . "/revisions/"
	    . intval($revprop->_number) . "/comments/",
            false, $context);   
        $revcom = json_decode(substr($revcom, 4));

        $threads = array();
        $commentid_to_threadid = array();
        foreach ($revcom as $filename=>$comments) {
            usort ($comments, "comment_comp");
            foreach ($comments as $comment) {
                if(isset($comment->in_reply_to))
                    $threadid = $comment->in_reply_to;
                else
                    $threadid = $comment->id;

                while (isset($commentid_to_threadid[$threadid])) {
                    
                    $newthreadid = $commentid_to_threadid[$threadid];
                    if($newthreadid == $threadid) break;
                    $threadid = $newthreadid;
                };
                $commentid_to_threadid[$comment->id] = $threadid;
                
                if(!isset($threads[$threadid]))
                {
                    $threads[$threadid] = array('comments'=>array(), 'done'=>false,
                        'id'=>$threadid, 'path'=>$filename, 'line'=>$comment->line);
                }
                $threads[$threadid]['comments'][] = $comment;

                if($comment->message == 'Done' ||
                   substr($comment->message, 0, strlen('No action')) == 'No action')
                {
                    $threads[$threadid]['done'] = true;
                }
                else
                {
                    // newer comment? mark as undone
                    $threads[$threadid]['done'] = false;
                }
            }    
        }

        if(!count($threads)) continue;

	$patchsets[$revprop->_number] = $threads;
    }

    $max_patchset = $revisions[0]->_number;

    echo '<ul>';
    foreach($patchsets as $number=>$patchset) {
	echo '<li><a href="#patchset' . $number . '">Patchset ' . $number . '</a></li>';
    }
    echo '</ul>';

    foreach ($patchsets as $number=>$patchset) {
	echo '<a name="patchset' . $number . '"></a><h3>Patchset ' . $number . '</h3>';
        echo '<a href="#top">top</a><br>';

        $threads = $patchset;
        foreach ($threads as $threadid=>$thread)
        {
            if($thread['done'])
            {
                echo '<div class="thread done">';
            }
            else echo '<div class="thread undone">';

            echo '<a target="_blank" href="' . $gerrit_url . '/#/c/' . $change . '/' . htmlspecialchars($number) . '..' . htmlspecialchars($max_patchset) . '/' . htmlspecialchars(urlencode($thread['path'])) . '@' . htmlspecialchars($thread['line']) . '">' . htmlspecialchars($thread['path']) . '@' . htmlspecialchars($thread['line']) . '</a><br>';


            foreach ($thread['comments'] as $comment)
            {
                echo '<b title="' . htmlspecialchars($comment->updated) . '">' . htmlspecialchars($comment->author->username) . '</b>: ';
                echo nl2br(str_replace('  ', ' &nbsp;', htmlspecialchars($comment->message)));
                echo '<br>';
            }

            echo '</div>';
	}
    }
}


if(!isset($_GET['chg']) && php_sapi_name() != "cli")
{
?>
<form method="get">
change id: <input name="chg"><br>
<input type="submit">
</form>
<?php
}
else
{
    if(php_sapi_name() == "cli")
	$_GET['chg'] = $argv[1];

    unhandled_comments($gerrit_base, intval(@$_GET["chg"]));
}

?>

</body></html>
